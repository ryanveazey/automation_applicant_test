Feature: More Google Searches

  Scenario: The Google homepage should have a Search link
    Given I am on the Google homepage
    Then I should see a Search link

  Scenario: The Google homepage should have links to other Google services
    Given I am on the Google homepage
    Then I should see a "Maps" link
    And I should see a "Play" link
    And I should see a "YouTube" link
    And I should see a "Gmail" link

  Scenario: I should be able to search for images
    Given I am on the Google homepage
    When I do an image search for "computers"
    Then I should see more than 10 images
