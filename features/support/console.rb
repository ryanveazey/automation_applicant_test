#This file should only run if it was required by pry (via "rake console")
if ENV['CONSOLE']
  require 'capybara'
  require 'pry'

  class World
    extend Capybara::DSL
  end

  def World(m)
    if m.is_a? Module
      World.extend m
    else
      puts "WARNING:: Cannot extend World with non-module in console"
    end
  end

  def After;end #do nothing, but do not crash plz

  World.pry
end
