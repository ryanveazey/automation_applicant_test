require ENV['CONSOLE'] ? 'capybara' : 'capybara/cucumber'
require 'pry'

Capybara.run_server = false
Capybara.default_driver = :selenium
