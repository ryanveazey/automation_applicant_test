Given(/^I am on the Google homepage$/) do
  visit 'http://www.google.com'
end

Then(/^I should see an Images link$/) do
  page.should have_link 'Images'
end

Then(/^I should see a button titled "(.*?)"$/) do |button_title|
  page.should have_button button_title
end

Given(/^I search for "(.*?)"$/) do |search_term|
  fill_in 'q', :with => search_term
  find('#gbqfb').click #Here we need to find the button by the ID and then click it
end

Then(/^I should see the text "(.*?)" in the result list$/) do |text_to_find|
  page.should have_text text_to_find
end

Then(/^I should see more than five pages of results$/) do
  find('#nav').all('a').length.should > 5
end

