Feature: Basic Google Searches

  Scenario: The Google homepage should have an Images link
    Given I am on the Google homepage
    Then I should see an Images link

  Scenario: The Google homepage should have Search and Feeling Lucky buttons
    Given I am on the Google homepage
    Then I should see a button titled "I'm Feeling Lucky"
    And I should see a button titled "Google Search" 

  Scenario: Google searches should return relevant results
    Given I am on the Google homepage
    When I search for "apples"
    Then I should see the text "apples" in the result list

  Scenario: Google searches for common words should return many results
    Given I am on the Google homepage
    When I search for "kitties"
    Then I should see more than five pages of results
