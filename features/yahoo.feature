Feature: Basic Yahoo Searches

  Scenario: The Yahoo homepage should have an Images link
    Given I am on the Yahoo homepage
    Then I should see a Finance link

  Scenario: The Yahoo homepage should have a Search button
    Given I am on the Yahoo homepage
    And I should see a button titled "Search" 

  Scenario: Yahoo searches should return relevant results
    Given I am on the Yahoo homepage
    When I yahoo search for "apples"
    Then I should see the text "apples" in the result list


 #Bonus: Write a new scenario, and step definitions, to confirm that
 #clicking on the jobs link (not just reading the href) takes you to http://jobsearch.monster.com (the rest of the url doesn't matter)
